#!/bin/sh

find "$@" -name \*.tex | while read file
do
#    date="$(echo "$file" | sed -e 's/.*\/\(\d{4}-\d{2}-\d{2}\).*/\1/')"
    date="$(echo "$file" | sed -e 's,.*/\([0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}\)[^/]*,\1,')"
    title="$(echo "$file" | sed -e 's,.*/[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}[ \t-]*\([^/]*\)\.tex,\1,')"
    echo "\\\\begin{blogentry}{$date}{$title}"
    echo "\\\\input{$file}"
    echo "\\\\end{blogentry}"
    echo
done
